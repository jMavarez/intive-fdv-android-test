package com.experiments.josue.randomuser.model

import android.os.Parcel
import android.os.Parcelable

data class RandomUser(
    val gender: String,
    val name: Name,
    val email: String,
    val picture: Picture,
    val location: Location
) : Parcelable {
  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.readParcelable<Name>(Name::class.java.classLoader),
      parcel.readString(),
      parcel.readParcelable<Picture>(Picture::class.java.classLoader),
      parcel.readParcelable<Location>(Location::class.java.classLoader))

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(gender)
    parcel.writeParcelable(name, 0)
    parcel.writeString(email)
    parcel.writeParcelable(picture, 0)
    parcel.writeParcelable(location, 0)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<RandomUser> {
    override fun createFromParcel(parcel: Parcel): RandomUser {
      return RandomUser(parcel)
    }

    override fun newArray(size: Int): Array<RandomUser?> {
      return arrayOfNulls(size)
    }
  }
}

data class Name(
    val title: String,
    val first: String,
    val last: String
) : Parcelable {
  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.readString(),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(title)
    parcel.writeString(first)
    parcel.writeString(last)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Name> {
    override fun createFromParcel(parcel: Parcel): Name {
      return Name(parcel)
    }

    override fun newArray(size: Int): Array<Name?> {
      return arrayOfNulls(size)
    }
  }
}

data class Picture(
    val large: String,
    val medium: String,
    val thumbnail: String
) : Parcelable {
  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.readString(),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(large)
    parcel.writeString(medium)
    parcel.writeString(thumbnail)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Picture> {
    override fun createFromParcel(parcel: Parcel): Picture {
      return Picture(parcel)
    }

    override fun newArray(size: Int): Array<Picture?> {
      return arrayOfNulls(size)
    }
  }
}

data class Location(
    val street: String,
    val city: String,
    val state: String
) : Parcelable {
  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.readString(),
      parcel.readString())

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(street)
    parcel.writeString(city)
    parcel.writeString(state)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Location> {
    override fun createFromParcel(parcel: Parcel): Location {
      return Location(parcel)
    }

    override fun newArray(size: Int): Array<Location?> {
      return arrayOfNulls(size)
    }
  }
}