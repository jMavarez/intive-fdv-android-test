package com.experiments.josue.randomuser.randomusers

import com.experiments.josue.randomuser.di.ActivityScoped
import dagger.Binds
import dagger.Module

@Module
abstract class RandomUsersModule {

  @ActivityScoped
  @Binds
  internal abstract fun randomUsersPresenter(presenter: RandomUsersPresenter): RandomUsersContract.Presenter

}