package com.experiments.josue.randomuser.randomusers

import android.content.Context
import android.content.Intent
import android.support.annotation.Nullable
import android.util.Log
import com.experiments.josue.randomuser.data.RandomUserApi
import com.experiments.josue.randomuser.model.RandomUser
import com.experiments.josue.randomuser.randomuserdetails.RandomUserDetailsActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RandomUsersPresenter @Inject constructor() : RandomUsersContract.Presenter {

  private var PAGE = 1
  private val RESULTS = 50
  private val SEED: String = "intive-fdv-android-test"

  private val disposable = CompositeDisposable()

  @Inject
  lateinit var randomUserApi: RandomUserApi

  @Nullable
  private var randomUsersView: RandomUsersContract.View? = null

  override fun loadUsers() {
    disposable.add(randomUserApi.getRandomUserLists(PAGE, RESULTS, SEED)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { data ->
          randomUsersView?.showUsers(data.results)
          PAGE += 1
        })
  }

  override fun openRandomUser(context: Context, randomUser: RandomUser) {
    val intent = Intent(context, RandomUserDetailsActivity::class.java)
    intent.putExtra("RandomUser", randomUser)
    context.startActivity(intent)
  }

  override fun takeView(view: RandomUsersContract.View) {
    randomUsersView = view
  }

  override fun dropView() {
    randomUsersView = null
    disposable.clear()
  }

}