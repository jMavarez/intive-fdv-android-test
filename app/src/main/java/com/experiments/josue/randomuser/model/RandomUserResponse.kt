package com.experiments.josue.randomuser.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RandomUserResponse(
    @SerializedName("results")
    @Expose
    var results: List<RandomUser>
)
