package com.experiments.josue.randomuser.randomuserdetails

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.experiments.josue.randomuser.R
import com.experiments.josue.randomuser.model.RandomUser
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.random_user_details_activity.*

class RandomUserDetailsActivity : AppCompatActivity() {

  @SuppressLint("SetTextI18n")
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.random_user_details_activity)

    val randomUser = intent.extras.getParcelable("RandomUser") as RandomUser

    setSupportActionBar(toolbar)

    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.title = "${randomUser.name.title.capitalize()} ${randomUser.name.first.capitalize()} ${randomUser.name.last.capitalize()}"

    Picasso.get()
        .load(randomUser.picture.large)
        .into(iv_user_image)

    tv_user_email.text = randomUser.email
    tv_user_location.text = "${randomUser.location.street}. ${randomUser.location.city.capitalize()}, ${randomUser.location.state.capitalize()}"
  }

  override fun onSupportNavigateUp(): Boolean {
    onBackPressed()
    return true
  }
}