package com.experiments.josue.randomuser

interface BasePresenter<T> {
  fun takeView(view: T)

  fun dropView()
}