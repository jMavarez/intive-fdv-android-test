package com.experiments.josue.randomuser.data

import com.experiments.josue.randomuser.model.RandomUserResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RandomUserApi {

  @Headers("Content-Type: application/json")
  @GET("api/")
  fun getRandomUserLists(@Query("page") page: Int, @Query("results") results: Int, @Query("seed") seed: String): Flowable<RandomUserResponse>

}
