package com.experiments.josue.randomuser.randomusers.adapter

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.experiments.josue.randomuser.R
import com.experiments.josue.randomuser.model.RandomUser

class RandomUsersAdapter(private val context: Context, private val itemListener: RandomUsersItemListener, recyclerView: RecyclerView) : RecyclerView.Adapter<RandomUserViewHolder>() {

  private var randomUsers: List<RandomUser> = emptyList()
  private lateinit var totalItems: Number
  private lateinit var lastVisibleItems: Number
  private val visibleThreshold = 6
  private var onLoadMoreListener: OnLoadMoreListener? = null
  private var loading = false

  init {
    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
      override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy > 0) {
          totalItems = (recyclerView.layoutManager as GridLayoutManager).itemCount
          lastVisibleItems = (recyclerView.layoutManager as GridLayoutManager).findLastVisibleItemPosition()

          if (!loading && totalItems as Int <= (lastVisibleItems as Int + visibleThreshold)) {
            onLoadMoreListener?.apply {
              onLoadMore()
            }

            loading = true
          }
        }
      }
    })
  }

  override fun onCreateViewHolder(parent: ViewGroup, position: Int): RandomUserViewHolder {
    return RandomUserViewHolder(LayoutInflater.from(context).inflate(R.layout.random_user_item, parent, false))
  }

  override fun getItemCount(): Int = randomUsers.size

  override fun onBindViewHolder(holder: RandomUserViewHolder, position: Int) {
    val randomUser = getItem(position)
    holder.bind(randomUser)

    holder.view.setOnClickListener {
      itemListener.onRandomUserClick(randomUser)
    }
  }

  private fun getItem(position: Int): RandomUser = randomUsers[position]

  fun bind(data: List<RandomUser>) {
    if (this.randomUsers.isEmpty()) {
      this.randomUsers = data
    } else {
      this.randomUsers += data
    }

    notifyDataSetChanged()
  }

  fun setLoaded() {
    this.loading = false
  }

  fun setOnLoadListener(onLoadMoreListener: OnLoadMoreListener) {
    this.onLoadMoreListener = onLoadMoreListener
  }

  interface OnLoadMoreListener {
    fun onLoadMore()
  }

}