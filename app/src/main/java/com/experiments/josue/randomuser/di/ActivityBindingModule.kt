package com.experiments.josue.randomuser.di

import com.experiments.josue.randomuser.randomusers.RandomUsersActivity
import com.experiments.josue.randomuser.randomusers.RandomUsersModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

  @ActivityScoped
  @ContributesAndroidInjector(modules = [(RandomUsersModule::class)])
  abstract fun randomUsersActivity(): RandomUsersActivity

}