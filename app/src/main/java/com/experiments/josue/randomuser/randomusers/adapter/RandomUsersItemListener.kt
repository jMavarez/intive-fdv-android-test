package com.experiments.josue.randomuser.randomusers.adapter

import com.experiments.josue.randomuser.model.RandomUser

interface RandomUsersItemListener {

  fun onRandomUserClick(randomUser: RandomUser)

}