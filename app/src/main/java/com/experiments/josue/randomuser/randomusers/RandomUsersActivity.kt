package com.experiments.josue.randomuser.randomusers

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.experiments.josue.randomuser.R
import com.experiments.josue.randomuser.model.RandomUser
import com.experiments.josue.randomuser.randomusers.adapter.RandomUsersAdapter
import com.experiments.josue.randomuser.randomusers.adapter.RandomUsersItemListener
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.random_users_activity.*
import javax.inject.Inject

class RandomUsersActivity : DaggerAppCompatActivity(), RandomUsersContract.View {

  @Inject
  lateinit var randomUsersPresenter: RandomUsersContract.Presenter

  lateinit var randomUsersAdapter: RandomUsersAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.random_users_activity)

    val itemListener = object : RandomUsersItemListener {
      override fun onRandomUserClick(randomUser: RandomUser) {
        randomUsersPresenter.openRandomUser(this@RandomUsersActivity, randomUser)
      }
    }

    val onLoadMoreListener = object : RandomUsersAdapter.OnLoadMoreListener {
      override fun onLoadMore() {
        Log.i("MAIN", "LOOOADDDDDD")
        randomUsersPresenter.loadUsers()
      }
    }

    val randomUserLayoutManager= GridLayoutManager(this, 3)
    randomUsersAdapter = RandomUsersAdapter(this, itemListener, random_users_recycler).apply {
      setOnLoadListener(onLoadMoreListener)
    }

    random_users_recycler.apply {
      layoutManager = randomUserLayoutManager
      adapter = randomUsersAdapter
      itemAnimator = DefaultItemAnimator().apply { supportsChangeAnimations = false }
    }

    randomUsersPresenter.loadUsers()
  }

  override fun showUsers(randomUsers: List<RandomUser>) {
    randomUsersAdapter.bind(randomUsers)
    randomUsersAdapter.setLoaded()
  }

  override fun setLoadingIndicator(active: Boolean) {

  }

  override fun onResume() {
    super.onResume()
    randomUsersPresenter.takeView(this)
  }

  override fun onStop() {
    super.onStop()
    randomUsersPresenter.dropView()
  }

}
