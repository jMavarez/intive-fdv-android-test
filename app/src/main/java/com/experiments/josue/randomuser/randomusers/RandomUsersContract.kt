package com.experiments.josue.randomuser.randomusers

import android.content.Context
import com.experiments.josue.randomuser.BasePresenter
import com.experiments.josue.randomuser.BaseView
import com.experiments.josue.randomuser.model.RandomUser

interface RandomUsersContract {
  interface View : BaseView<Presenter> {
    fun showUsers(randomUsers: List<RandomUser>)

    fun setLoadingIndicator(active: Boolean)
  }

  interface Presenter : BasePresenter<View> {
    fun loadUsers()

    fun openRandomUser(context: Context, randomUser: RandomUser)
  }
}