package com.experiments.josue.randomuser.di

import android.support.annotation.NonNull
import android.util.Log
import com.experiments.josue.randomuser.data.RandomUserApi
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import javax.inject.Singleton

@Module
class RetrofitModule {

  private val BASEURL = "https://randomuser.me/"

  @Provides
  @Singleton
  @NonNull
  fun provideOkHttp(): OkHttpClient.Builder = OkHttpClient.Builder()
      .addInterceptor(LoggingInterceptor())

  @Provides
  @Singleton
  fun provideRandomUserApi(okHttpClientBuilder: OkHttpClient.Builder): RandomUserApi {
    val gson = GsonBuilder()
        .setLenient()
        .create()

    return Retrofit.Builder()
        .baseUrl(BASEURL)
        .callFactory(okHttpClientBuilder.build())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()
        .create(RandomUserApi::class.java)
  }

}

internal class LoggingInterceptor : Interceptor {
  @Throws(IOException::class)
  override fun intercept(chain: Interceptor.Chain): Response {
    val request = chain.request()

    val t1 = System.nanoTime()
    Log.i("HTTP", String.format("Sending request %s on %s%n%s",
        request.url(), chain.connection(), request.headers()))

    val response = chain.proceed(request)

    val t2 = System.nanoTime()
    Log.i("HTTP", String.format("Received response for %s in %.1fms%n%s",
        response.request().url(), (t2 - t1) / 1e6, response.headers()))

    return response
  }
}
