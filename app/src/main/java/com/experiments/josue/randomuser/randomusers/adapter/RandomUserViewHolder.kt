package com.experiments.josue.randomuser.randomusers.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import com.experiments.josue.randomuser.model.RandomUser
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.random_user_item.view.*

class RandomUserViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

  fun bind(user: RandomUser) {
    Picasso.get()
        .load(user.picture.large)
        .into(view.iv_random_user)
  }

}