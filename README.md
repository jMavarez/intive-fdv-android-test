## MOBILE TEST

Code challenge!
Create a simple app using the Random User API.

- The app should have a Master/Detail flow in which at least 50 profile image thumbnails are shown on a grid on a “home” screen.
- When randomUsers tap on a thumbnail on the grid, they’re taken into a new screen in which the large image is shown on the top of the screen, and some basic user data is listed below: username, first name, last name, and email
address.

Here is the [API documentation](https://randomuser.me/documentation)

Some tips:

- We recommend using one of the [Android Architecture Blueprints](https://github.com/googlesamples/android-architecture) as a starting point, but it’s up to you.

- You can implement some kind of pagination or “infinite scroll” on the grid but it’s not mandatory.

- If you have some extra time or you just want to show off, you can add some bells and whistles to your app:
transition animations, image masks, visual response to touch, and so on.
